package newpackage;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationAutomate {

	static WebDriver driver = null;
	DataFormatter df;

	public void GoogleSignup() throws Exception {

//		  System.setProperty("webdriver.gecko.driver","/usr/local/bin/geckodriver" );
//		  driver=new FirefoxDriver();
//		  driver.manage().window().maximize();
//		  String baseUrl = "https://accounts.google.com/SignUp?service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ltmpl=default";
//		    driver.get(baseUrl);
//
//		    
		File src = new File("/home/nirjhar/Desktop/testData/TestData.xlsx");
		FileInputStream fis = new FileInputStream(src);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet1 = wb.getSheetAt(0);

		String data0 = sheet1.getRow(0).getCell(0).getStringCellValue();
		String data1 = sheet1.getRow(0).getCell(1).getStringCellValue();
//      	 int data2=(int)sheet1.getRow(0).getCell(2).getNumericCellValue();
//      	 
//      	 int data3=(int)sheet1.getRow(0).getCell(3).getNumericCellValue();

		String data2 = sheet1.getRow(0).getCell(2).getStringCellValue();

		String data3 = sheet1.getRow(0).getCell(3).getStringCellValue();
		String data4 = sheet1.getRow(0).getCell(4).getStringCellValue();

		// By ID Text area1
		WebElement text1 = driver.findElement(By.id("firstName"));
		text1.clear();
		text1.sendKeys(data0);

//		    
		WebElement text2 = driver.findElement(By.xpath(".//*[@id='lastName']"));
		text2.clear();
		text2.sendKeys(data1);

		WebElement text3 = driver.findElement(By.xpath(".//*[@id='username']"));
		text3.clear();
		text3.sendKeys(data4);

		WebElement text4 = driver.findElement(By.xpath(".//*[@name='Passwd']"));
		text4.clear();
		text4.sendKeys(String.valueOf(data2));

		WebElement text5 = driver.findElement(By.xpath(".//*[@name='ConfirmPasswd']"));
		text5.clear();
		text5.sendKeys(String.valueOf(data3));

//		    WebElement text6 = driver.findElement(By.id("BirthDay"));
//		    text6.clear();
//		    text6.sendKeys("1");
//
//		    WebElement text7 = driver.findElement(By.id("BirthYear"));
//		    text7.clear();
//		    text7.sendKeys("2000");
//
//		    WebElement text8 = driver.findElement(By.id("RecoveryPhoneNumber"));
//		    text8.clear();
//		    text8.sendKeys("9222103436");
//
//		    WebElement text9 =  driver.findElement(By.id("RecoveryEmailAddress"));
//		    text9.clear();
//		    text9.sendKeys("abc_gh@yahoo.com");

//		    googleSelect(By.id("Gender"), "Male");
//
//		    googleSelect(By.id("BirthMonth"), "March");
//
//		    googleSelect(By.xpath(".//*[@id='CountryCode']/div"), "United States");

		WebElement text10 = driver.findElement(By.xpath("//span[contains(text(), 'Next')]"));
		text10.click();

		driver.quit();

		wb.close();
	}

	private static void googleSelect(By by, String text) {
		driver.findElement(by).click();

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(driver
				.findElement(By.xpath("//div[@class='goog-menu goog-menu-vertical']//div[text()='" + text + "']"))));
		driver.findElement(By.xpath("//div[@class='goog-menu goog-menu-vertical']//div[text()='" + text + "']"))
				.click();

	}

	public static void main(String[] args) throws Exception {
		RegistrationAutomate ra = new RegistrationAutomate();
		ra.GoogleSignup();

//	}
	}

}
