package newpackage;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SecureSignLogin {
	static WebDriver driver = null;
	String useName;
	String pass;

	public void secureLogin() throws Exception {
		System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		//String baseUrl = "http://localhost:3000/";
		String baseUrl="http://0dd6ddfc.ngrok.io/";
		driver.get(baseUrl);

		File src = new File("/home/nirjhar/Desktop/testData/TestData.xlsx");
		FileInputStream fis = new FileInputStream(src);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet1 = wb.getSheetAt(2);

		int rowcount = sheet1.getLastRowNum();

		System.out.println("Total no of rows " + rowcount);
		
		for(int i=0;i<=rowcount;i++)
		{
			
			useName = sheet1.getRow(i).getCell(0).getStringCellValue();
			pass=new DataFormatter().formatCellValue(sheet1.getRow(i).getCell(1));
			//pass = sheet1.getRow(i).getCell(1).getStringCellValue();
			
			
			WebElement text3 = driver.findElement(By.xpath(".//*[@name='emailid']"));
			text3.clear();
			text3.sendKeys(useName);
			
			
			WebElement text5 = driver.findElement(By.xpath(".//*[@name='password']"));
			text5.clear();
			text5.sendKeys(pass);
			
			WebElement text10 = driver.findElement(By.xpath("//span[contains(text(), 'Login')]"));
			text10.click();
			
			
			Thread.sleep(2000);
			Alert alert = driver.switchTo().alert();
			// Prints text and closes alert
			System.out.println(alert.getText());
			alert.accept();

			
			driver.get(baseUrl);
		}
		
		
	}

	public static void main(String[] args) throws Exception {
		
		SecureSignLogin login=new SecureSignLogin();
		login.secureLogin();
		
	}

}
