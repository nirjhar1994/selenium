package newpackage;

import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SecureSignRegistration {

	static WebDriver driver = null;
	DataFormatter df;
	String firstname;
	String lastname;
	String email;
	String mobile;;
	String pass;
	String delimiter = "@";
	String temp[];

	public void secureRegistration() throws Exception {
		System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		String baseUrl = "http://localhost:3000/register";

		// String baseUrl="http://0dd6ddfc.ngrok.io/register";
		driver.get(baseUrl);

		File src = new File("/home/nirjhar/Desktop/testData/TestData.xlsx");
		FileInputStream fis = new FileInputStream(src);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet1 = wb.getSheetAt(1);

		int rowcount = sheet1.getLastRowNum();

		System.out.println("Total no of rows " + rowcount);
		for (int i = 0; i <= rowcount; i++) {
			firstname = sheet1.getRow(i).getCell(0).getStringCellValue();
			lastname = sheet1.getRow(i).getCell(1).getStringCellValue();
			email = sheet1.getRow(i).getCell(2).getStringCellValue();

			mobile = new DataFormatter().formatCellValue(sheet1.getRow(i).getCell(3));
			pass = sheet1.getRow(i).getCell(4).getStringCellValue() + "";
			System.out.println(firstname + " " + lastname + " " + email + " " + mobile + " " + pass);
			WebElement text1 = driver.findElement(By.xpath(".//*[@name='username']"));
			text1.clear();
			text1.sendKeys(firstname);

			WebElement text2 = driver.findElement(By.xpath(".//*[@name='username1']"));
			text2.clear();
			text2.sendKeys(lastname);

			WebElement text3 = driver.findElement(By.xpath(".//*[@name='emailid']"));
			text3.clear();
			text3.sendKeys(email);

			WebElement text4 = driver.findElement(By.xpath(".//*[@name='mobileno']"));
			text4.clear();
			text4.sendKeys(mobile);
			WebElement text5 = driver.findElement(By.xpath(".//*[@name='password']"));
			text5.clear();
			text5.sendKeys(pass);

//	 		    
			WebElement text10 = driver.findElement(By.xpath("//span[contains(text(), 'Register')]"));
			text10.click();

			Thread.sleep(2000);
			Alert alert = driver.switchTo().alert();
			// Prints text and closes alert
			System.out.println(alert.getText());
			alert.accept();
			// driver.navigate().refresh();

			((JavascriptExecutor) driver).executeScript("window.open()");
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1)); // switches to new tab

			temp = email.split(delimiter);
			System.out.println("Domain name is " + temp[0]);

			driver.get("https://www." + temp[1]);
//
			WebElement emailMailinator = driver.findElement(By.id("addOverlay"));
			emailMailinator.clear();
			emailMailinator.sendKeys(email);

			WebElement goToMail = driver.findElement(By.id("go-to-public"));

			goToMail.click();

			// driver.switchTo().frame("msg_body");

			WebElement secureSignActivate = driver.findElement(By.partialLinkText("SecureSign - Activation"));
			secureSignActivate.click();
			// WebElement iframe = driver.findElement(By.tagName("iframe"));

//			
			// driver.switchTo().frame(iframe);

//			WebDriverWait wait = new WebDriverWait(driver, 30);
//			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("msg_body")));
			// System.out.println(driver.switchTo().frame(iframe));

			// driver.findElement(By.xpath("//a[contains(text(),'click here')]")).click();
			// driver.findElement(By.partialLinkText("click here")).click();

//		   driver.findElement(By.xpath("//html/body/a")).click();;

			// driver.findElement(By.cssSelector("[a*=\"http://localhost:3000/activate?actHash\"]")).click();

//			driver.findElement(By.partialLinkText("key/url?url")).click();
			
			
			Thread.sleep(3000);
			
			((JavascriptExecutor) driver).executeScript("window.open()");
			ArrayList<String> tab1 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tab1.get(2)); // switches to new tab

			
			Thread.sleep(1000);
			driver.navigate().to("http://localhost:3000/activate?actHash");
			// driver.get(baseUrl);
			

			//driver.get("http://localhost:3000/activate?actHash");
			WebElement activation=driver.findElement(By.xpath("//*[@id='main-registration-container']/h6/a"));
			activation.click();
			
			
			
			WebElement username = driver.findElement(By.xpath(".//*[@name='emailid']"));
			username.clear();
			username.sendKeys(email);
			
			
			WebElement password = driver.findElement(By.xpath(".//*[@name='password']"));
			password.clear();
			password.sendKeys(pass);
			
			WebElement loginButton = driver.findElement(By.xpath("//span[contains(text(), 'Login')]"));
			loginButton.click();
			
			
			Thread.sleep(2000);
			Alert alert1 = driver.switchTo().alert();
			// Prints text and closes alert
			System.out.println(alert1.getText());
			alert1.accept();


		}
		driver.quit();
		wb.close();

	}

	public static void main(String[] args) throws Exception {
		SecureSignRegistration sign = new SecureSignRegistration();
		sign.secureRegistration();

	}

}
